--
-- Excercise 0
--
-- natToInteger :: Nat -> Integer
--
module Main where
import Data.List
import Data.Char
import Hugs.IOExts (unsafeCoerce)

data Nat = Zero | Succ Nat deriving Show

natToInteger Zero = 0
natToInteger (Succ n) = natToInteger n + 1

-- ok
natToInteger1 Zero = 0
natToInteger1 (Succ n) = natToInteger1 n + 1

-- ok
natToInteger2 (Succ n) = natToInteger2 n + 1
natToInteger2 Zero = 0

-- not ok
natToInteger3 n = natToInteger3 n

-- ok
natToInteger4 (Succ n) = 1 + natToInteger4 n
natToInteger4 Zero = 0

-- not ok
natToInteger5 Zero = 1
natToInteger5 (Succ n) = (1 + natToInteger5 n) - 1

-- ok
natToInteger6 = head . m
    where m Zero = [0]
          m (Succ n) = [sum [x | x <- (1 : m n)]]

-- ok
natToInteger7 :: Nat -> Integer
natToInteger7 = \n -> genericLength [c | c <- show n, c == 'S']

-- not ok
--natToInteger8 :: Nat -> Integer
--natToInteger8 = \n -> length [c | c <- show n, c == 'S']

--
-- Excercise 1
--
-- integerToNat :: Integer -> Nat
--

integerToNat (n+1) = Succ (integerToNat n)
integerToNat 0 = Zero

-- not ok
-- XXX: according to hw, this is actually ok
integertoNat1 0 = Zero
integerToNat1 (n+1) = Succ (integerToNat1 n)

-- not ok
integertoNat2 0 = Succ Zero
integerToNat2 n = (Succ (integertoNat2 n))

-- not ok
integerToNat3 n = product [(unsafeCoerce c) :: Integer | c <- show n]

-- not ok
integerToNat4 n = integerToNat4 n

-- ok
integerToNat5 (n+1) = Succ (integerToNat5 n)
integerToNat5 0 = Zero

-- ok
integerToNat6 (n+1) = let m = integerToNat6 n in Succ m
integerToNat6 0 = Zero

-- not ok
integerToNat7 = head . m
    where {
          ; m 0 = [0]
          ; m (n + 1) = [sum [x | x <- (1 : m n)]]
          }

-- not ok, type error
--integerToNat8 :: Integer -> Nat
--integerToNat8 = \n -> genericLength [c | c <- show n, isDigit c]


--
-- Excercise 2
--
-- add :: Nat -> Nat -> Nat
--

add Zero n = n
add (Succ m) n = Succ (add n m)

-- ok
add1 Zero n = n
add1 (Succ m) n = Succ (add1 n m)

-- ok
add2 (Succ m) n = Succ (add2 n m)
add2 Zero n = n

-- ok
add3 Zero n = Zero
add3 (Succ m) n = Succ (add3 n m)

-- not ok
add4 (Succ m) n = Succ (add4 m n)
add4 Zero n = Zero

-- not ok
add5 n Zero = Zero
add5 n (Succ m) = Succ (add5 n m)

-- not ok
add6 n (Succ m) = Succ (add6 n m)
add6 n Zero = Zero

-- ok
add7 n Zero = n
add7 n (Succ m) = Succ (add7 m n)

-- ok
add8 n (Succ m) = Succ (add8 m n)
add8 n Zero = n

--
-- Excercise 3
--
-- mult :: Nat -> Nat -> Nat
--

-- not ok
mult1 Zero Zero = Zero
mult1 m (Succ n) = add m (mult1 m n)

-- ok
mult2 m Zero = Zero
mult2 m (Succ n) = add m (mult2 m n)

-- not ok
mult3 m Zero = Zero
mult3 m (Succ n) = add n (mult3 m n)

-- not ok
mult4 m Zero = Zero
mult4 m n = add m (mult4 m (Succ n))

--
-- Excercise 4
--
-- The standard library defines the following algebraic data type to represent
-- the possible comparisons between two values.

--data Ordering = LT
--              | EQ
--              | GT

-- together with a function:

--compare :: (Ord a) => a -> a -> Ordering

-- that decides if a value x :: Ord a => a is less than (LT), equal to (EQ), or
-- greater than (GT) another value y :: Ord a => a.
--
-- Given the following data type for trees with Integers at the leafs and inside
-- the nodes: 

data Tree = Leaf Integer
          | Node Tree Integer Tree

-- Select all correct implementations of the function:

-- occurs :: Integer -> Tree -> Bool

-- that decides whether the given Integer occurs in the given Tree

-- ok
occurs1 m (Leaf n) = m == n
occurs1 m (Node l n r)
    = case compare m n of
        LT -> occurs1 m l
        EQ -> True
        GT -> occurs1 m r

-- not ok
occurs2 m (Leaf n) = m == n
occurs2 m (Node l n r)
    = case compare m n of
        LT -> occurs2 m r
        EQ -> True
        GT -> occurs2 m l

-- not ok
--occurs3 m (Leaf n) = compare m n
--occurs3 m (Node l n r)
--    = case compare m n of
--        LT -> occurs3 m l
--        EQ -> True
--        GT -> occurs3 m r

-- ok
-- XXX: wasn't really ok
-- ... it was the EQ -> False part :(
occurs4 m (Leaf n) = m == n
occurs4 m (Node l n r)
    = case compare m n of
        LT -> occurs4 m l
        EQ -> False
        GT -> occurs4 m r

-- ok
occurs5 m (Leaf n) = m == n
occurs5 m (Node l n r)
    | m == n = True
    | m < n = occurs5 m l
    | otherwise = occurs5 m r

-- not ok
occurs6 m (Leaf n) = m == n
occurs6 m (Node l n r)
    | m == n = True
    | m > n = occurs6 m l
    | otherwise = occurs6 m r

-- not ok
--occurs7 m n = m == n
--occurs7 m (Node l n r)
--    | m == n = True
--    | m < n = occurs7 m l
--    | otherwise = occurs7 m r

-- not ok
--occurs8 m n = m == n
--occurs8 m (Node l n r)
--    | m == n = False
--    | m < n = occurs8 m r
--    | otherwise = occurs8 m l


testTree :: Tree
testTree = Node (Leaf 1) 2 (Node (Leaf 3) 4 (Leaf 5))

--
-- Excercise 5
--
-- balanced :: Tree -> Bool
--

data Tree2 = Leaf2 Integer
           | Node2 Tree2 Tree2
           deriving Show

testTree2 :: Tree2
testTree2 = Node2 (Leaf2 1) (Node2 (Leaf2 3) (Leaf2 5))

balanced :: Tree2 -> Bool
leaves (Leaf2 _) = 1
leaves (Node2 l r) = leaves l + leaves r
balanced (Leaf2 _) = True
balanced (Node2 l r)
    = abs (leaves l - leaves r) <= 1 && balanced l && balanced r

--
-- Excercise 6
--
-- balance :: [Integer] -> Tree
--

halve xs = splitAt (length xs `div` 2) xs
balance [x] = Leaf2 x
balance xs = Node2 (balance ys) (balance zs)
    where (ys, zs) = halve xs

