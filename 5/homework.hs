-- 
-- Excercise 0
-- 
-- (^) :: (Num a, Integral b) => a -> b -> a
-- 

-- not ok
m `f1` 0 = 0
m `f1` n = m * m `f1` (n - 1)

-- ok
m `f2` 0 = 1
m `f2` n = m * m `f2` (n - 1)

-- not ok
m `f3` 0 = 1
m `f3` n = m * m `f3` n - 1

-- not ok
m `f4` 0 = 1
m `f4` n = n * n `f4` (m - 1)

-- ok
m `f5` 0 = 1
m `f5` n = m * f5 m (n - 1)

-- not ok
m `f6` 0 = 1
m `f6` n = m * m * m `f6` (n - 2)

-- not ok
m `f7` 0 = 1
m `f7` n = (m * m) `f7` (n - 1)

-- not ok (zero case)
m `f8` 1 = m
m `f8` n = m * m `f8` (n - 1)

--
-- Excercise 4
-- 
-- and :: [Bool] -> Bool
-- 

-- ok
and1 [] = True
and1 (b : bs) = b && and1 bs

-- ok
and2 [] = True
and2 (b : bs)
    | b = and2 bs
    | otherwise = False

-- not ok
and3 [] = False
and3 (b : bs) = b && and3 bs

-- not ok
and4 [] = False
and4 (b : bs) = b || and4 bs

-- ok
and5 [] = True
and5 (b : bs)
    | b == False = False
    | otherwise = and5 bs

-- not ok
and6 [] = True
and6 (b : bs) = b || and6 bs

-- ok
and7 [] = True
and7 (b : bs) = and7 bs && b

-- not ok
and8 [] = True
and8 (b : bs)
    | b = b
    | otherwise = and8 bs

--
-- Excercise 5
-- 
-- concat :: [[a]] -> [a]
-- 

-- not ok
concat1 [] = []
concat1 (xs : xss) = xs : concat1 xss

-- ok
concat2 [] = []
concat2 (xs : xss) = xs ++ concat2 xss

-- not ok
concat3 [] = [[]]
concat3 (xs : xss) = xs ++ concat3 xss

-- not ok
concat4 [[]] = []
concat4 (xs : xss) = xs ++ concat4 xss


--
-- Excercise 6
-- 
-- replicate :: Int -> a -> [a]
--

-- not ok
--replicate1 1 x = x
--replicate1 n x = x : replicate1 (n - 1) x

-- not ok
--replicate2 0 _ = []
--replicate2 n x = replicate2 (n - 1) x : x

-- not ok (wrong base case)
replicate3 1 _ = []
replicate3 n x = replicate3 (n - 1) x ++ [x]

-- ok
replicate4 0 _ = []
replicate4 n x = x : replicate (n - 1) x


--
-- Excercise 7
--
-- (!!) :: [a] -> Int -> a 
--

-- not ok
(x : _) `get1` 1 = x
(_ : xs) `get1` n = xs `get1` (n + 1)

-- not ok
(x : _) `get2` 0 = x
(_ : xs) `get2` n = xs `get2` (n - 1)
[] `get2` n = 0

-- ok
(x : _ ) `get3` 0 = x
(_ : xs) `get3` n = xs `get3` (n - 1)

-- not ok
(x : _) `get4` 0 = [x]
(_ : xs) `get4` n = xs `get4` (n - 1)


--
-- Excercise 8
--
-- elem :: Eq a => a -> [a] -> Bool
--

-- ok
elem1 _ [] = False
elem1 x (y : ys)
    | x == y = True
    | otherwise = elem1 x ys

-- not ok
elem2 _ [] = False
elem2 x (y : ys)
    | x == y = True
    | otherwise = elem2 x (y : ys)

-- not ok
elem3 _ [] = True
elem3 x (y : ys)
    | x == y = True
    | otherwise = elem3 x ys

-- not ok
elem4 _ [] = False
elem4 x (y : ys) = x == y

--
-- Excercise 9
--
-- merge :: Ord a => [a] -> [a] -> [a]
-- 
-- that merges two sorted lists in ascending order to give a single sorted list
-- in ascending order
--
-- > merge [2, 5, 6] [1, 3, 4]
-- [1, 2, 3, 4, 5, 6]
--

-- not ok
merge1 [] ys = ys
merge1 xs [] = xs
merge1 (x : xs) (y : ys) = if x <= y then x : merge1 xs ys else y : merge1 xs ys


-- not ok
merge2 [] ys = ys
merge2 xs [] = xs
merge2 (x : xs) (y : ys) = if x <= y then y : merge2 xs (y : ys) else x : merge2 (x : xs) ys

-- not ok
merge3 [] ys = ys
merge3 xs [] = xs
merge3 (x : xs) (y : ys) = if x <= y then y : merge3 (x : xs) ys else x : merge3 xs (y : ys)

-- ok
merge4 [] ys = ys
merge4 xs [] = xs
merge4 (x : xs) (y : ys) = if x <= y then x : merge4 xs (y : ys) else y : merge4 (x : xs) ys


--
-- Excercise 10
--
-- msort :: Ord a => [a] -> [a]
--
-- that implements merge sort, in which the empty list and singleton lists are
-- already sorted, and any other list is sorted by merging together the two
-- lists that result from sorting the two halves of the list separately. 
--
-- The solutions can use the function merge from the previous exercise and the
-- function halve that splits a list into two halves whose lengths differ by at
-- most one. 
--

halve :: [a] -> ([a], [a])
halve xs = splitAt (length xs `div` 2) xs

merge [] ys = ys
merge xs [] = xs
merge (x : xs) (y : ys) = if x <= y then x : merge xs (y : ys) else y : merge (x : xs) ys

-- not ok
msort1 [] = []
msort1 xs =  merge (msort1 zs) (msort1 ys)
    where (ys, zs) = halve xs

-- ok
msort2 [] = []
msort2 [x] = [x]
msort2 xs = merge (msort2 ys) (msort2 zs)
    where (ys, zs) = halve xs

-- not ok
msort3 [] = []
msort3 [x] = [x]
msort3 xs = msort3 ys ++ msort3 zs
    where (ys, zs) = halve xs

-- not ok
msort4 [] = []
msort4 [x] = [x]
msort4 xs = msort4 (msort4 ys ++ msort4 zs)
    where (ys, zs) = halve xs


