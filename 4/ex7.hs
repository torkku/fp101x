--
-- Excercise 7
--
-- Modify the Caesar cipher program to also handle upper-case letters.
-- Given the following string Think like a Fundamentalist Code like a
-- Hacker, encode it with your modified program (using shift size 13)
-- and choose the correct output.
--

import Data.Char

char2int base c = ord c - ord base
int2char base n = chr (ord base + n)

--let2int :: Char -> Int
--let2int c = ord c - ord 'a'
let2int = char2int 'a'
cap2int = char2int 'A'

--int2let :: Int -> Char
--int2let n = chr (ord 'a' + n)
int2let = int2char 'a'
int2cap = int2char 'A'

shift :: Int -> Char -> Char
shift n c
    | isLower c = int2let ((let2int c + n) `mod` 26)
    | isUpper c = int2cap ((cap2int c + n) `mod` 26)
    | otherwise = c

encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs]

