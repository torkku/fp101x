--
-- Excercise 0
--
-- Calculate the sum 1^2 + 2^2 + ... + 100^2
--

sum100 = sum [ x^2 | x <- [1 .. 100]] 


--
-- Excercise 1
--
-- > replicate 3 True
-- [True, True, True]
-- 

replicate n a = [ a | _ <- [1..n]]


--
-- Excercise 2
--
-- A triple (x, y, z) of positive integers is pythagorean if x^2 + y^2 = z^2
-- 
-- function pyths :: Int -> [(Int, Int, Int)] that returns the list of all
-- pythagorean triples whose components are at most a given limit. 
--

pyths n = [(x, y, z) | x <- [1..n], y <- [1..n], z <- [1..n], x^2 + y^2 == z^2]


--
-- Excercise 3
--
-- A positive integer is perfect if it equals the sum of its factors, excluding
-- the number itself. 
--
-- function perfects :: Int -> [Int] that returns the list of all perfect
-- numbers up to a given limit.
--

isFactorOf x n = n `mod` x == 0
factors n = [ x | x <- [1..n], isFactorOf x n ]

perfects n = [x | x <- [1..n], isPerfect x]
    where isPerfect num = sum (init (factors num)) == num


--
-- Excercise 4
--
-- [(x, y) | x <- [1,2,3], y <- [4,5,6]]
-- can be re-expressed using two or more comprehensions with single generators.
-- 

--concat [[(x, y) | y <- [4, 5, 6]] | x <- [1, 2, 3]]

--
-- Excercise 5
--
-- Redefine the function positions discussed in the lecture, using the function find: 
--
find ::  (Eq a) => a -> [(a, b)] -> [b]
find k t = [v | (k', v) <- t, k == k']

--positions :: (Eq a) => a -> [a] -> [Int]

-- ok
positions1 x xs = find x (zip xs [0..n])
    where n = length xs - 1

-- not ok
positions2 x xs = find x xs

-- not ok
--positions3 x xs = find x (zipWith (+) xs [0..n])
--    where n = length xs - 1

-- not ok
positions4 x xs = find n (zip xs [0..x])
    where n = length xs - 1

--
-- Excercise 6
--
-- The scalar product of two lists of integers xs and ys of length n is given by the sum of the products of corresponding integers:
-- sum ( (xs !! i) * (ys !! i) ) for i = 0 to n-1
--
-- scalarproduct :: [ Int ] -> [ Int ] -> Int that returns the scalar product of two lists.
--
-- > scalarproduct [1, 2, 3] [4, 5, 6]
-- 32

scalarproduct xs ys = sum [x * y | (x, y) <- xs `zip` ys]

--
-- Excercise 7
--
-- ex7.hs
--


--
-- Excercise 13
--
-- function divisors :: Int -> [Int] that returns the divisors of a natural number.
--
-- divisors 15 = [1, 3, 5, 15]
--
-- The function divides :: Int -> Int -> Bool decides if one integer is divisible by another.
--

divides :: Int -> Int -> Bool
divides n m = n `mod` m == 0

divisors :: Int -> [Int]
divisors x = [d | d <- [1..x], x `divides` d]


