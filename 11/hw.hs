--
-- Excercise 6
--
-- fibs :: [Integer] that generates the infinite sequence of Fibonacci numbers (i.e. [0, 1, 1, 2, ...]).
--

fibs = 0 : 1 : [ x + y | (x, y) <- zip fibs (tail fibs)]

--
-- Excercise 7
--
-- fib :: Int -> Integer that returns the n-th Fibonnaci number (counting from zero).
--

fib n = fibs !! n

--
-- Excercise 8
--
-- largeFib :: Integer that uses fibs from the previous exercises to calculate the first Fibonacci number greater than 1000
--

largeFib = head (dropWhile (<= 1000) fibs)

--
-- Excercise 9
--
--  repeatTree :: a -> Tree a for the following type of binary trees:
--

data Tree a = Leaf
            | Node (Tree a) a (Tree a)

-- The behavior should be analogous to that of the library function repeat (not considering bottom and partial cases):
-- repeat :: a -> [a]
-- repeat x = xs
--   where xs = x : xs

repeatTree x = Node t x t
    where t = repeatTree x

