
-- "A Poor Man's Concurrency Monad"
-- source code, to be used with the Functional Pearl.
--
-- Koen Claessen, May 16th, 1997.
-------------------------------------------------------------------

{-
  To be used with Hugs1.4.

  Changes to the code in the paper we had to make:
    - We use restricted type synonyms, with explicit function names;
    - The function `round' is renamed to `roundr';
    - The class `Monoidal' is split up in `MonadZero' and `MonadPlus'.
-}

import IOExts

-------------------------------------------------------------------
-- 2. Monads

-- writer monad

class Monad m => Writer m where
  write :: String -> m ()

type W a = (a, String)
  in bindW, returnW, writeW, output

bindW            :: W a -> (a -> W b) -> W b
(a, s) `bindW` k  = let (b, s') = k a in (b, s++s')

returnW          :: a -> W a
returnW x         = (x, "")

instance Monad W where
  m >>= k  = m `bindW` k
  return x = returnW x

writeW   :: String -> W ()
writeW s  = ((), s)

instance Writer W where
  write s = writeW s

output        :: W a -> String
output (a, s)  = s

-- monad transformer

class MonadTrans t where
  lift :: Monad m => m a -> (t m) a

-------------------------------------------------------------------
-- 3. Concurrency

-- 3.1 continuations

type C m a = (a -> Action m) -> Action m
  in bindC, returnC
   , action, stop, atom, par, fork, run

bindC       :: C m a -> (a -> C m b) -> C m b
m `bindC` k  = \c -> m (\a -> k a c)

returnC     :: a -> C m a
returnC x    = \c -> c x

instance Monad m => Monad (C m) where
  m >>= k  = m `bindC` k
  return x = returnC x

-- 3.2 the type Action

data Action m
  = Atom (m (Action m))
  | Fork (Action m) (Action m)
  | Stop

action    :: Monad m => C m a -> Action m
action m   = m (\a -> Stop)

atom      :: Monad m => m a -> C m a
atom m     = \c -> Atom (do a <- m ; return (c a))

stop      :: Monad m => C m a
stop       = \c -> Stop

par       :: Monad m => C m a -> C m a -> C m a
par m1 m2  = \c -> Fork (m1 c) (m2 c)

fork      :: Monad m => C m a -> C m ()
fork m     = \c -> Fork (action m) (c ())

instance MonadTrans C where
  lift = atom

-- 3.3 semantics

roundr :: Monad m => [Action m] -> m ()
roundr []     = return ()
roundr (a:as) = case a of
  Atom am    -> do a <- am ; roundr (as ++ [a])
  Fork a1 a2 -> roundr (as ++ [a1,a2])
  Stop       -> roundr as

run   :: Monad m => C m a -> m ()
run m  = roundr [action m]

-------------------------------------------------------------------
-- 4. Examples

-- 4.1 concurrent output

{-
instance Writer m => Writer (C m) where
  write s = lift (write s)
-}

loop    :: Writer m => String -> m ()
loop s   = do write s ; loop s

example :: Writer m => C m ()
example  = do write "start!"
              fork (loop "fish")
              loop "cat"

instance Writer m => Writer (C m) where
  write []    = return ()
  write (c:s) = do lift (write [c]) ; write s

-- 4.2 merging of infinite lists

instance Monad m => MonadZero (C m) where
  zero = stop

instance Monad m => MonadPlus (C m) where
  (++) = par

merge :: [String] -> String
merge  = output . run . concat . map write

-- 4.3 concurrent state

type Var a = IORef a

newVar   = newIORef undefined
readVar  = readIORef
writeVar = writeIORef

type MVar a = Var (Maybe a)
  in newMVar, writeMVar, takeVar, readMVar

newMVar       :: C IO (MVar a)
newMVar        = lift ( do v <- newVar
                           writeVar v Nothing
                           return v )

writeMVar     :: MVar a -> a -> C IO ()
writeMVar v a  = lift (writeVar v (Just a))

takeVar       :: MVar a -> IO (Maybe a)
takeVar v      = do am <- readVar v
                    writeVar v Nothing
                    return am

readMVar      :: MVar a -> C IO a
readMVar v     = do am <- lift (takeVar v)
                    case am of
                      Nothing -> readMVar v
                      Just a  -> return a

{-
-- (a more efficient implementation of concurrent state,
--  inspired by the Hugs implementation.)

type MVar a = Var (Either a [a -> Action IO])
  in newMVar, writeMVar, readMVar

-- Left a   - indicates that there is value a.
-- Right ws - indicates that the MVar is empty and that ws are the processes
--   that are blocking on the MVar.

-- add writeMVar and readMVar to the restricted type C!

newMVar       :: C IO (MVar a)
newMVar        = lift ( do v <- newVar
                           writeVar v (Right [])
                           return v )

writeMVar     :: MVar a -> a -> C IO ()
writeMVar v a  = \c -> Atom (
                 do ea <- readVar v
                    case ea of
                      Left _       -> error "putMVar on a full MVar"
                      Right []     -> do writeVar v (Left a)
                                         return (c ())
                      Right (w:ws) -> do writeVar v (Right ws)
                                         return (Fork (w a) (c ())) )

readMVar      :: MVar a -> C IO a
readMVar v     = \c -> Atom (
                 do ea <- readVar v
                    case ea of
                      Left a       -> do writeVar v (Right [])
                                         return (c a)
                      Right ws     -> do writeVar v (Right (ws ++ [c]))
                                         return Stop )
-}

-------------------------------------------------------------------
-- the end.



