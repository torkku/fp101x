--
-- continuation passing style
--

--
-- pythagoras
--

add :: Int -> Int -> Int
add x y = x + y

square :: Int -> Int
square x = x*x

-- a^2 + b^2 = c^2
pyth :: Int -> Int -> Int
pyth x y = add (square x) (square y)

-- 
-- Main> pyth 3 4
-- 25
-- 

add_cps :: Int -> Int -> ((Int -> r) -> r)
add_cps x y = \k -> k (add x y)

square_cps :: Int -> ((Int -> r) -> r)
square_cps x = \k -> k (square x)

pyth_cps :: Int -> Int -> ((Int -> r) -> r)
pyth_cps x y = \k ->
    square_cps x $ \x_squared ->
    square_cps y $ \y_squared ->
    add_cps x_squared y_squared $ k

--
-- Main> pyth_cps 3 4 print
-- 25
-- 

--
-- thrice
--

thrice :: (a -> a) -> a -> a
thrice f x = f (f (f x))

-- 
-- Main> thrice (\x -> x + 1) 1
-- 4
-- 

-- 
-- A higher order function such as thrice, when converted to CPS, takes as
-- arguments functions in CPS form as well. 
--
-- Therefore, f :: a -> a will become:
--
--   f_cps :: a -> ((a -> r) -> r)
--
-- and the final type will be:
--
-- thrice_cps :: (a -> ((a -> r) -> r)) -> a -> ((a -> r) -> r)
--

thrice_cps :: (a -> ((a -> r) -> r)) -> a -> ((a -> r) -> r)
thrice_cps f_cps x = \k ->
    f_cps x $ \fx ->
    f_cps fx $ \ffx ->
    f_cps ffx $ k

-- 
-- Main> thrice_cps (\x k -> k (x + 1)) 1 print
-- 4
-- 
-- Main> thrice_cps (\x k -> k (x + 2)) 1 print
-- 7
-- 


-- 
-- The Cont Monad
--
-- Having continuation-passing functions, the next step is providing a neat way
-- of composing them, preferably one which does not require the long chains of
-- nested lambdas we have seen just above. A good start would be a combinator
-- for applying a CPS function to a suspended computation. A possible type for
-- it would be:
-- 
-- chainCPS :: ((a -> r) -> r) -> (a -> ((b -> r) -> r)) -> ((b -> r) -> r)
--
-- Hint: start by stating that the result is a function which takes a b -> r
-- continuation; then, let the types guide you.
--

chainCPS :: ((a -> r) -> r) -> (a -> ((b -> r) -> r)) -> ((b -> r) -> r)
chainCPS s f = \k -> s $ \x -> f x $ k   

--
-- We supply the original suspended computation s with a continuation which
-- makes a new suspended computation (produced by f) and passes the final
-- continuation k to it.
--
-- All we need now is a Cont r a type to wrap suspended computations, with the
-- usual wrapper and unwrapper functions.
--

newtype Cont r a = Cont { runCont :: ((a -> r) -> r) }

cont :: ((a -> r) -> r) -> Cont r a
cont f = Cont f


--
-- m >>= k = Cont $ \c -> runCont m $ \a -> runCont (k a) c
-- 
-- m >>= k = let s c = runCont m c
--               t c = \a -> runCont (k a) c
--           in  Cont $ \c -> s (t c)
--

instance Monad (Cont r) where
    return x = cont ($ x)
    s >>= f  = cont $ \c -> runCont s $ \x -> runCont (f x) c


add_cont :: Int -> Int -> Cont r Int
add_cont x y = return (add x y)

square_cont :: Int -> Cont r Int
square_cont x = return (square x)

--final_cont :: Show a => a -> String
--final_cont x = "Done: " ++ show (x)

pythagoras_cont :: Int -> Int -> Cont r Int
pythagoras_cont x y = do x_squared <- square_cont x
                         y_squared <- square_cont y
			 add_cont x_squared y_squared

