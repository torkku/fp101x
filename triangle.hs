type Triangle = (Int, Int, Int)
type MegaTriangle = [Triangle]

rotate :: Triangle -> Triangle
rotate t = case t of
	     (p1, p2, p3) -> (p2, p3, p1)

bottom t = case t of
	    (_, _, x) -> x

left t = case t of
	    (x, _, _) -> x

right t = case t of
	    (_, x, _) -> x

initTriangle :: MegaTriangle
initTriangle = [(1, -1, 2), (2, -1, -3),(2, -1, 1),(2, -2, -1),(2, -3, -2),(-3, -1, -3),(-1, 3, 1),(0, 1, 0),(-1, -2, 2)]

validTriangle :: MegaTriangle
validTriangle = [(-1, 2, -2), (-1, 3, -3), (-1, -3, 2), (1, -1, 2), (-2, 1, -2), (1, -1, 3), (-1, 1, 3), (2, -1, -2), ( -2, 2, -3)]


isValid :: MegaTriangle -> Bool
isValid [] = True
isValid (p1:p2:p3:p4:p5:p6:p7:p8:p9:[]) =
    -- corners
    abs (bottom p1) == abs (bottom p3)
    && abs (bottom p1) == abs (bottom p3)
    && abs (right  p8) == abs (right  p7)
    && abs (left   p5) == abs (right  p9)
    -- middle               
    && abs (right p2) == abs (right p3)
    && abs (left   p3) == abs (right   p7)
    && abs (bottom p7) == abs (bottom p6)
    && abs (right  p6) == abs (left   p2)
    && abs (left  p2) == abs (left   p1)
    && abs (bottom p1) == abs (bottom p8)
    && abs (right p8) == abs (right p7)
    && abs (left p7) == abs (left p2)


p1 :: Triangle
p1 = (1, -1, 2)

p2 :: Triangle
p2 = (2, -1, -3)

p3 :: Triangle
p3 = (2, -1, 1)

p4 :: Triangle
p4 = (2, -2, -1)

p5 :: Triangle
p5 = (2, -3, -2)

p6 :: Triangle
p6 = (-3, -1, -3)

p7 :: Triangle
p7 = (-1, 3, 1)

p8 :: Triangle
p8 = (0, 1, 0)

p9 :: Triangle
p9 = (-1, -2, 2)


