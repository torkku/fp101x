import qualified Data.Foldable as F

------------------------------------------------------------------------------------------------------------------------------
-- ROSE TREES, FUNCTORS, MONOIDS, FOLDABLES
------------------------------------------------------------------------------------------------------------------------------

data Rose a = a :> [Rose a] deriving Show

-- ===================================
-- Ex. 0-2
--
-- Write the functions:
--
-- root :: Rose a -> a
-- children :: Rose a -> [Rose a]
--
-- that return the value stored at the root of a rose tree, respectively the
-- children of the root of a rose tree.
--
-- root (1 :> [2 :> [], 3 :> []]) = 1
-- root ('a' :> []) = 'a'
-- children (1 :> [2 :> [], 3 :> []]) = [2 :> [], 3 :> []]
-- children ('a' :> []) = []
--
-- ===================================

root :: Rose a -> a 
root (x :> _) = x

children :: Rose a -> [Rose a]
children (_ :> xs) = xs

xs = 0 :> [1 :> [2 :> [3 :> [4 :> [], 5 :> []]]], 6 :> [], 7 :> [8 :> [9 :> [10 :> []], 11 :> []], 12 :> [13 :> []]]]

ex0 = let tree = 'x' :> map (flip (:>) []) ['a'..'x'] in length $ children tree
ex1 = let tree = 'x' :> map (\c -> c :> []) ['a'..'A'] in length (children tree)
ex2 = root . head . children . head . children . head . drop 2 $ children xs

-- ===================================
-- Ex. 3-7
-- 
-- Write the functions
--
-- size :: Rose a -> Int
-- leaves :: Rose a -> Int
--
-- that count the number of nodes in a rose tree, respectively the number of
-- leaves (nodes without any children).
--
-- ===================================

size :: Rose a -> Int
size tree = 1 + sum (map size (children tree))

leaves :: Rose a -> Int
leaves (_ :> []) = 1
leaves tree = sum (map leaves (children tree))

ex3 = let tree = 1 :> map (\c -> c :> []) [1..5] in size tree
ex4 = let tree = 1 :> map (\c -> c :> []) [1..5] in size . head . children $ tree
ex5 = let tree = 1 :> map (\c -> c :> []) [1..5] in leaves tree
ex6 = let tree = 1 :> map (\c -> c :> []) [1..5] in product (map leaves (children tree))
ex7 = (*) (leaves . head . children . head . children $ xs) (product . map size . children . head . drop 2 . children $ xs)

-- ===================================
-- Ex. 8-10
-- 
-- Similarly to how we might want to apply a function uniformly to all elements
-- in a list, we might also want to apply a function uniformly to all the
-- elements in a rose tree, or any other container-like data structure for that
-- matter.
--
-- For this purpose Haskell has a Functor type class, exposing a single function
-- fmap that generalizes the map function:
--
-- class Functor f where 
--   fmap :: (a -> b) -> f a -> f b
--
-- We see that fmap generalizes map by giving a Functor instance for lists:
--
-- instance Functor [] where
--   fmap = map
--
-- Verify that fmap and map have the same type if we instantiate f to []
--
-- Write a Functor instance for the Rose data type.
--
--  For example:
--
--  fmap (*2) (1 :> [2 :> [], 3 :> []]) = (2 :> [4 :> [], 6 :> []])
--  fmap (+1) (1 :> []) = (2 :> []) 
--
-- ===================================

--
-- http://stackoverflow.com/questions/29972130/haskell-mapping-a-function-over-a-rose-tree
--
-- Ocaml:
-- 
-- type 'a tree = Node of ('a * 'a tree list)
--
-- map ~f (Node (label, forest)) =
--   Node (f label, List.map ~f:(map ~f) forest)
--
--
--map' :: (a -> b) -> Rose a -> Rose b
--map' f (label :> []) = f label :> []
--map' f (label :> forest) = (f label) :> (map' f forest)
--
--data Rose' a = RoseCtr a [Rose' a]
--
--size' :: Rose' a -> Int
--size' (RoseCtr x xs) = 1 + size' xs
--
--leaves :: Rose a -> Int
--leaves (_ :> []) = 1
--leaves tree = sum (map leaves (children tree))


instance Functor Rose where
  fmap f (x :> xs) = (f x) :> map (fmap f) xs

ex8 = let tree = 1 :> map (\c -> c :> []) [1..5] in size (fmap leaves (fmap (:> []) tree))

-- ex9
-- Given the function f r = fmap head $ fmap (\x -> [x]) r, what is a valid type
-- signature for f?

ex9 r = fmap head $ fmap (\x -> [x]) r

-- Main> :t ex9
-- ex9 :: Functor a => a b -> a b
--
-- wrong, correct is Rose a -> Rose a ??
-- 

ex10 = round . root . head . children . fmap (\x -> if x > 0.5 then x else 0) $ fmap (\x -> sin(fromIntegral x)) xs

-- ===================================
-- Ex. 11-13
-- 
-- A Monoid is an algebraic structure over a type m with a single associative
-- binary operation mappend :: m -> m -> m and an identity element mempty :: m.
--
-- class Monoid m where
--   mempty :: m
--   mappend :: m -> m -> m
--
-- For example, lists are Monoids:
--
-- instance Monoid [] where
--   mempty = []
--   xs `mappend` ys = xs ++ ys
--
-- As an exercise:
--
-- 1) Verify that (++) is an associative operation (i.e. that for any xs ys zs, (xs
-- ++ ys) ++ zs = xs ++ (ys ++ zs))
-- 
-- 2) Verify that the empty list [] is indeed an identity element with respect to
-- list concatenation (++) (i.e. that for any list ls, [] ++ ls = ls and ls ++
-- [] = ls).
--
-- Numbers also form a Monoid, both under addition with 0 as the identity
-- element, and under multiplication with 1 as the identity element (verify
-- this).
--
-- However, we are only allowed to give one instance per combination of type and
-- type class. To overcome this limitation we create some newtype wrappers:
--
--  newtype Sum a = Sum a deriving Show
--  newtype Product a = Product a deriving Show 
--
-- Before proceeding, implement two unwrap functions unSum :: Sum a -> a and
-- unProduct :: Product a -> a that remove the newtype wrappers.
--
-- Complete the Monoid instances for Sum and Product in the template.
--
-- ===================================

class Monoid m where
  mempty :: m
  mappend :: m -> m -> m

newtype Sum a = Sum a
newtype Product a = Product a

instance Num a => Monoid (Sum a) where
  mempty = Sum 0
  mappend (Sum x) (Sum y) = Sum (x + y)
  
instance Num a => Monoid (Product a) where
  mempty = Product 1 
  mappend (Product x) (Product y) = Product (x * y) 

unSum :: Sum a -> a
unSum (Sum a) = a 
unProduct :: Product a -> a
unProduct (Product a) = a 

num1 = mappend (mappend (Sum 2) (mappend (mappend mempty (Sum 1)) mempty)) (mappend (Sum 2) (Sum 1))
  
num2 = mappend (Sum 3) (mappend mempty (mappend (mappend (mappend (Sum 2) mempty) (Sum (-1))) (Sum 3)))
  
ex11 = unProduct (Product 6 `mappend` (Product . unSum $ Sum 3 `mappend` Sum 4)) 
ex13 = unSum (mappend (Sum 5) (Sum (unProduct (mappend (Product (unSum num2)) (mappend (Product (unSum num1)) (mappend mempty (mappend (Product 2) (Product 3))))))))

-- ===================================
-- Ex. 14-15
--
-- If f is some container-like data structure storing elements of type m that
-- form a Monoid, then there is a way of folding all the elements in the data
-- structure into a single element of the monoid m. The following declaration
-- defines the type class Foldable:
--
-- class Functor f => Foldable f where fold :: Monoid m => f m -> m
--
-- Here is an example of how to make [a] an instance of Foldable:
--
-- instance Foldable [] where fold = foldr (mappend) mempty
--
-- Write a Foldable instance for Rose.
--
-- ===================================

class Functor f => Foldable f where
  fold :: Monoid m => f m -> m
  foldMap :: Monoid m => (a -> m) -> (f a -> m)
  foldMap f t = fold $ fmap f t 
  
instance Foldable Rose where
  fold (x :> []) = x `mappend` mempty
  fold (x :> xs) = f(g(xs)) `mappend` x
    where f = foldr (mappend) mempty
	  g = map (fold) 
  
sumxs = Sum 0 :> [Sum 13 :> [Sum 26 :> [Sum (-31) :> [Sum (-45) :> [], Sum 23 :> []]]], Sum 27 :> [], Sum 9 :> [Sum 15 :> [Sum 3 :> [Sum (-113) :> []], Sum 1 :> []], Sum 71 :> [Sum 55 :> []]]]

ex14 = let tree = 1 :> [2 :> [], 3 :> [4 :> []]] in
       let tree' = fmap Product tree  in
       unProduct $ fold tree'

ex15 = unSum (mappend (mappend (fold sumxs) (mappend (fold . head . drop 2 . children $ sumxs) (Sum 30))) (fold . head . children $ sumxs))

-- ===================================
-- Ex. 16-18
--
-- It might be the case that we have a foldable data structure storing elements
-- of type a that do not yet form a Monoid, but where we do have a function of
-- type Monoid m => a -> m that transforms them into one. To this end it would
-- be convenient to have a function foldMap :: Monoid m => (a -> m) -> f a -> m
-- that first transforms all the elements of the foldable into a Monoid and
-- then folds them into a single monoidal value.
--
-- Add a default implementation of foldMap to the Foldable type class,
-- expressed in terms of fold and fmap.
--
-- ===================================

ex16 = let tree = 42 :> [3 :> [2:> [], 1 :> [0 :> []]]] in unSum $ foldMap Sum tree

ex17 = unSum (mappend (mappend (foldMap (\x -> Sum x) xs) (mappend (foldMap (\x -> Sum x) . head . drop 2 . children $ xs) (Sum 30))) (foldMap (\x -> Sum x) . head . children $ xs))

ex18 = unSum (mappend (mappend (foldMap (\x -> Sum x) xs) (Sum (unProduct (mappend (foldMap (\x -> Product x) . head . drop 2 . children $ xs) (Product 3))))) (foldMap (\x -> Sum x) . head . children $ xs))

-- ===================================
-- Ex. 19-21
--
-- Write functions fsum, fproduct :: (Foldable f , Num a) => f a -> a that
-- compute the sum, respectively product, of all numbers in a foldable data
-- structure.
--
-- ===================================

fproduct, fsum :: (Foldable f, Num a) => f a -> a
fsum = unSum . foldMap Sum
fproduct = unProduct . foldMap Product

ex21 = ((fsum . head . drop 1 . children $ xs) + (fproduct . head . children . head . children . head . drop 2 . children $ xs)) - (fsum . head . children . head . children $ xs)

