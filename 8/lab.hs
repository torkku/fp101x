module Lab4 where

------------------------------------------------------------------------------------------------------------------------------
-- RECURSIVE FUNCTIONS
------------------------------------------------------------------------------------------------------------------------------

import Data.Char

-- ===================================
-- Ex. 0
-- Define a recursive function by induction over integers triangle :: Integer ->
-- Integer such that triangle n returns the sum of all numbers in the list
-- [0..n], where n >= 0
--
-- we would like you to define this as a recursive function of the shape:
--
-- triangle 0 = ...
-- triangle (n + 1) = ... (n + 1) ... triangle n ...
--
-- ===================================

triangle :: Integer -> Integer
triangle 0 = 0
triangle (n + 1) = (n + 1) + triangle n

--triangle :: Integer -> Integer
--triangle 0 = 0
--triangle n = n + triangle (n - 1)

-- ===================================
-- Ex. 3
-- Define a recursive function count :: Eq a => a -> [a] -> Integer that counts
-- how many times a given value occurs in a list
--
-- but we would like you to define this as a recursive function of the shape:
--
-- count a [] = ...
-- count a (x:xs) = ... a ... x ... count a xs ...
--
-- Example: count "Haskell" ["Java", "PHP", "Javascript", "C#"] = 0
-- Example: count 'e' "The quick brown fox jumped over the lazy dog." = 4
--
-- ===================================

--count :: Eq a => a -> [a] -> Int
count n [] = 0
count n (x : xs) = if n == x then 1 + count n xs else count n xs

xs = [1,2,35,2,3,4,8,2,9,0,5,2,8,4,9,1,9,7,3,9,2,0,5,2,7,6,92,8,3,6,1,9,2,4,8,7,1,2,8,0,4,5,2,3,6,2,3,9,8,4,7,1,4,0,1,8,4,1,2,4,56,7,2,98,3,5,28,4,0,12,4,6,8,1,9,4,8,62,3,71,0,3,8,10,2,4,7,12,9,0,3,47,1,0,23,4,8,1,20,5,7,29,3,5,68,23,5,6,3,4,98,1,0,2,3,8,1]
ys = map (\x -> ((x + 1) * 3) ^ 3 - 7) xs

poem = [ "Three Types for the Lisp-kings under the parentheses,"
       , "Seven for the Web-lords in their halls of XML,"
       , "Nine for C Developers doomed to segfault,"
       , "One for the Dark Lord on his dark throne"
       , "In the Land of Haskell where the Monads lie."
       , "One Type to rule them all, One Type to find them,"
       , "One Type to bring them all and in the Lambda >>= them"
       , "In the Land of Haskell where the Monads lie."
       ]

-- ===================================
-- Ex. 6
-- Euclid’s Algorithm returns the greatest common factor of two integers n and m
-- where both n and m are greater than 0. The algorithm repeatedly subtracts the
-- smaller number from the larger until the two numbers are equal, and then
-- returns that number. 
--
-- Example: euclid (5,7) = euclid (5,2) = euclid (3,2) = euclid (1,2) = euclid (1,1) = 1
-- Example: euclid (4, 2) = euclid (2, 2) = 2
--
-- ===================================

euclid :: (Int,  Int) -> Int
euclid (x, y)
    | x == y = x
    | x < y = euclid (y, x)
    | x > y = euclid (x - y, y)

-- ===================================
-- Ex. 7
-- Define a recursive function funkyMap :: (a -> b) -> (a -> b) -> [a] -> [b]
-- that takes as arguments two functions f and g and a list xs, and applies f to
-- all elements at even positions ([0, 2..]) in xs and g to all elements at odd
-- positions ([1, 3..]) in xs.
--
-- funkyMap (+10) (+100) [1, 2, 3, 4, 5] = [(+10) 1, (+100) 2, (+10) 3, (+100) 4, (+10) 5].
-- 
-- ===================================

funkyMap :: (a -> b) -> (a -> b) -> [a] -> [b]
funkyMap f g xs = funkyMapEven f g xs

funkyMapEven f g xs =
    case xs of
        [] -> []
        x : xs -> (f x) : funkyMapOdd f g xs

funkyMapOdd f g xs =
    case xs of
        [] -> []
        x : xs -> (g x) : funkyMapEven f g xs

--
-- Excercises 8..
--

--
-- What is the type of \ a -> a
--
-- Lab4> :t \a -> a
-- \a -> a :: a -> a
--

-- 
-- What is the type of [undefined]
--
-- Lab4> :t [undefined]
-- [undefined] :: [a]
--

-- What is the type of (True, (False))
--
-- Lab4> :t (True, (False))
-- (True,False) :: (Bool,Bool)
--

-- What is the type of f a = \b -> (b, a)
--
-- Lab4> :t \a -> \b -> (b, a)
-- \a -> \b -> (b,a) :: a -> b -> (b,a)
--

-- What is the type of foldr id
--
-- Lab4> :t foldr id
-- foldr id :: a -> [a -> a] -> a
--

-- What is the type of flip foldr const
-- 
-- Lab4> :t flip foldr const
-- flip foldr const :: (a -> (b -> c -> b) -> b -> c -> b) -> [a] -> b -> c -> b
--

-- Define dup a = (a, a)
-- What is the type of dup . dup . dup
--
--

dup a = (a, a)

--
-- Lab4> :t dup . dup . dup
-- dup . dup . dup :: a -> (((a,a),(a,a)),((a,a),(a,a)))
--

-- Define h g f = (f . g) $ f
-- What is the type of h

h g f = (f . g) $ f

-- Lab4> :t h
-- h :: ((a -> b) -> a) -> (a -> b) -> b


-- Given the function h defined in the previous question, define the function
-- fix = h fix

fix = h fix

-- Lab4> :T fix
-- fix :: (a -> a) -> a
--

-- Given the function fix defined in the previous question, select all
-- properties that fix satisfies:
--
-- - fix is a higher order function
-- - fix is a polymorphic function
-- - fix is recursive function
-- - fix is NOT an overloaded function
--

-- What is the type of
-- f = \f n -> if (n == 0) then 1 else n * f (n - 1)
-- 
-- Lab4> :t \f n -> if (n == 0) then 1 else n * f (n - 1)
-- \f n -> if n == 0 then 1 else n * f (n - 1) :: Num a => (a -> a) -> a -> a
--

-- Given the function f defined in the previous exercise, define the function 
-- k = fix $ f
-- 

k = fix $ \f n -> if (n == 0) then 1 else n * f (n - 1)

-- What is the value of k 42
-- Lab4> k 42
-- 1405006117752879898543142606244511569936384000000000


-- Given the function k defined in the previous exercise, select all properties
-- that k satisfies:
--
-- - k is a higher order function
-- - k is NOT a polymorphic function
-- - k is recursive function
-- - k is NOT an overloaded function
-- 
-- Correct answer:
-- - k is a polymorphic function
-- - k is a overloaded function
--

