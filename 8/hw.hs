import Control.Monad

--
-- Excercise 1
--
-- putStr' :: String -> IO ()
--

--putStr' [] = return ()
--putStr' (x : xs) = do
--                    putChar x
--                    putStr' xs
--                    return ()

putStr' [] = return ()
putStr' (x : xs) = putChar x >> putStr' xs


--
-- Excercise 2
--
-- putStrLn' :: String -> IO ()
--

-- ok
putStrLn1 [] = putChar '\n'
putStrLn1 xs = putStr' xs >> putStrLn1 ""

-- ok
putStrLn2 [] = putChar '\n'
putStrLn2 xs = putStr' xs >> putChar '\n'

-- ok
putStrLn3 [] = putChar '\n'
putStrLn3 xs = putStr' xs >>= \x -> putChar '\n'

--putStrLn4 [] = putChar '\n'
--putStrLn4 xs = putStr' xs >> \x -> putChar '\n'

-- ok
putStrLn5 [] = putChar '\n'
putStrLn5 xs = putStr' xs >> putStr' "\n"

-- not ok
putStrLn6 [] = putChar '\n'
putStrLn6 xs = putStr' xs >> putStrLn6 "\n"

-- not ok
--putStrLn7 [] = return ""
--putStrLn7 xs = putStrLn7 xs >> putStr' "\n"

-- not ok
--putStrLn8 [] = putChar "\n"
--putStrLn8 xs = putStr' xs >> putChar '\n'


--
-- Excercise 3
--
-- getLine' :: IO String
--

-- not ok
getLine1 = get1 ""

get1 :: String -> IO String
get1 xs
    = do x <- getChar
         case x of
             '\n' -> return xs
             _ -> get1 (x:xs)

-- ok
getLine2 = get2 []


get2 :: String -> IO String
get2 xs
    = do x <- getChar
         case x of
            '\n' -> return xs
            _ -> get2 (xs ++ [x])

test1
    = do xs <- getLine1
         putStr xs

test2
    = do xs <- getLine2
         putStr xs


--
-- Excercise 4
--
-- interact' :: (String -> String) -> IO ()
--
-- that takes as its argument a function of type String -> String, and reads a
-- line from the standard input, and passes it to this function, and then prints
-- the resulting output followed by a newline on the standard output
--

-- try
--interact' :: (String -> String) -> IO ()
--interact' f
--    = do xs <- getLine
--         let xs' = f xs in putStrLn xs'

interact' f
    = do input <- getLine
         putStrLn (f input)


--
-- Excercise 5
--
-- sequence_' :: Monad m => [m a] -> m ()
--
-- that takes a finite, non-partial, list of non-bottom, monadic values, and
-- evaluates them in sequence, from left to right, ignoring all (intermediate)
-- results
--

-- not ok
--sequence_1 [] = return []
--sequence_1 (m : ms) = m >> \_ -> sequence_1 ms

sequence_2 [] = return ()
sequence_2 (m : ms ) = (foldl (>>) m ms) >> return ()

test_sequence_2 = do
    rs <- sequence_2 [putChar 'a', putChar 'b', putChar 'c']
    print rs

-- not ok
--sequence_3 ms = foldl (>>) (return ()) ms
--
--test_sequence_3 = do
--    rs <- sequence_3 [putChar 'a', putChar 'b', putChar 'c']
--    print rs


sequence_4 [] = return ()
sequence_4 (m : ms) = m >> sequence_4 ms

test_sequence_4 = do
    rs <- sequence_4 [putChar 'a', putChar 'b', putChar 'c']
    print rs

sequence_5 [] = return ()
sequence_5 (m : ms) = m >>= \_ -> sequence_5 ms

test_sequence_5 = do
    rs <- sequence_5 [putChar 'a', putChar 'b', putChar 'c']
    print rs

-- not ok
--sequence_6 ms = foldr (>>=) (return ()) ms

sequence_7 ms = foldr (>>) (return ()) ms

test_sequence_7 = do
    rs <- sequence_7 [putChar 'a', putChar 'b', putChar 'c']
    print rs

-- not ok
--sequence_8 ms = foldr (>>) (return []) ms
--
--test_sequence_8 = do
--    rs <- sequence_8 [putChar 'a', putChar 'b', putChar 'c']
--    print rs

-- ALL NOT OK???
-- NOPE. looks like 2,4,5,7 are ok!
-- better test with put than get ??


--
-- Excercise 7
--
-- sequence' :: Monad m => [m a] -> m [a]
-- 
-- collecting all (intermediate) results into a list
--
--

--
sequence1 [] = return []
sequence1 (m : ms) = m >>=
    \a ->
	do as <- sequence1 ms
	   return (a : as)

test_sequence1 = do
    rs <- sequence1 [getLine, getLine, getLine]
    print rs

-- not ok, type error
--sequence2 ms = foldr func (return ()) ms
--    where
--	func :: (Monad m) => m a -> m [a] -> m [a]
--	func m acc
--	    = do x <- m
--	         xs <- acc
--		 return (x : xs)

-- not ok, type error
--sequence3 ms = foldr func (return []) ms
--    where
--	func :: (Monad m) => m a -> m [a] -> m [a]
--	func m acc = m : acc

-- not ok, mixing do syntax with no do
--sequence4 [] = return []
--sequence4 (m : ms) = return (a : as)
--    where
--	a <- m
--	as <- sequence4 ms


-- ok! goddamn, missed this one
sequence5 ms = foldr func (return []) ms
    where
	func :: (Monad m) => m a -> m [a] -> m [a]
	func m acc
	    = do x <- m
	         xs <- acc
		 return (x : xs)

-- not ok, type error
--sequence6 [] = return []
--sequence6 (m : ms)
--    = m >>
--	\a ->
--	    do as <- sequence6 ms
--	       return (a : as)

-- not ok, mixing do syntax with no do
--sequence7 [] = return []
--sequence7 (m : ms) = 
--    m >>= \a ->
--	as <- sequence7 ms
--	return (a : as)

sequence8 [] = return []
sequence8 (m : ms)
    = do a <- m
         as <- sequence8 ms
	 return (a : as)

test_sequence8 = do
    rs <- sequence8 [getLine, getLine, getLine]
    print rs

--
-- Excercise 7
--
-- mapM' :: Monad m => (a -> m b) -> [a] -> m [b] 
--

-- ok
mapM1 f as = sequence (map f as)

-- not ok, type error
-- THIS WAS NOT A TYPE ERROR, BUT A WORKING SOLUTION???
mapM2 f [] = return []
mapM2 f (a : as)
    = f a >>= \b -> mapM2 f as >>= \bs -> return (b : bs)

-- not ok, throws away output
mapM3 f as = sequence_ (map f as)

-- not ok, >> doesnt bind a value
--mapM4 f [] = return []
--mapM4 f (a : as)
--    = f a >> \b -> mapM4 f as >> \bs -> return (b : bs)

-- not ok, bad do notation
--mapM5 f [] = return []
--mapM5 f (a : as) =
--    do
--	f a -> b
--	mapM5 f as -> bs
--	return (b : bs)

-- ok
mapM6 f [] = return []
mapM6 f (a : as)
    = do b <- f a
         bs <- mapM6 f as
	 return (b : bs)

-- ok 
mapM7 f [] = return []
mapM7 f (a : as)
    = f a >>=
	\b ->
	    do bs <- mapM7 f as
	       return (b : bs)

-- not ok, wrong return order because of append
mapM8 f [] = return []
mapM8 f (a : as)
    = f a >>=
	\b ->
	    do bs <- mapM8 f as
	       return (bs ++ [b])

test1_mapM f xs = do
    output <- f (putStrLn . show) xs
    putStrLn (show output)

test2_mapM f xs = do
    f (\x -> putStrLn (show x) >> return (x + 1)) xs

test3_mapM f xs = do
    output <- f (\x -> putChar 'a' >> return (x + 1)) xs
    putStrLn (show output)

--
-- Excercise 8
--
-- filterM' :: Monad m => (a -> m Bool) -> [a] -> m [a]
--

-- not ok, doesn't filter
filterM1 _ [] = return []
filterM1 p (x : xs)
    = do flag <- p x
         ys <- filterM1 p xs
	 return (x : xs)

-- ok
filterM2 _ [] = return []
filterM2 p (x : xs)
    = do flag <- p x
         ys <- filterM2 p xs
	 if flag then return (x : ys) else return ys

-- not ok, p x not returning a Monad
filterM3 _ [] = return []
filterM3 p (x : xs)
    = do ys <- filterM3 p xs
         if p x then return (x : ys) else return ys

-- not ok, reverse filter
filterM4 _ [] = return []
filterM4 p (x : xs)
    = do flag <- p x
         ys <- filterM4 p xs
	 if flag then return ys else return (x : ys)

test1_filterM f = f (\i -> if i <= 0 then Nothing else return (even i))

--
-- Excercise 9
--
-- foldLeftM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
--
-- that takes an accumulation function a -> b -> m a, and a seed of type a
-- and left folds a finite, non-partial list of non-bottom elements of type
-- b into a single result of type m a
--
-- Hint: The recursive structure of foldLeftM looks as follows: 
--
-- foldLeftM f a [] = ... a ...
-- foldLeftM f a (x:xs) = foldLeftM f (... f ... a ... (>>=) ...) xs 
--
-- Remember the definition of foldl: 
--
-- foldl f a [] = ... a ...
-- foldl f a (x:xs) = foldl f (... f ... a ...) xs 
--
-- foldl implementation:
-- foldl f a [] = a
-- foldl f a (x:xs) = foldl f (f a x) xs
-- 

foldLeftM :: (Monad m) => (a -> b -> m a) -> a -> [b] -> m a

-- works, yet bit confused why the other versions don't
foldLeftM f a [] = return a
foldLeftM f a (x:xs)
    = f a x >>= \r -> foldLeftM f r xs

-- fourth try, seems to work? nope.
--foldLeftM f acc [] = return acc
--foldLeftM f acc (x:xs)
--    = (f acc x) >>= \fax -> do foldLeftM f fax xs
--                               return fax

-- second fixed, does not work
--foldLeftM f a [] = return a
--foldLeftM f a (x:xs)
--    = do acc <- f a x
--         foldLeftM f acc xs
--         return acc

-- BELOW:
--
-- *** Term           : foldLeftM
-- *** Type           : (a -> b -> c a) -> a -> [b] -> a
-- *** Does not match : (a -> b -> c a) -> a -> [b] -> c a

-- first try:
--foldLeftM f a [] = a
--foldLeftM f a (x:xs)
--    = f a x >>= \r ->
--                    do rs <- foldLeftM f a xs
--                       return (r : rs)

-- second try:
--foldLeftM f a [] = a
--foldLeftM f a (x:xs)
--    = do r <- f a x
--         rs <- foldLeftM f a xs
--         return (r : rs)

-- third try:
--foldLeftM f a [] = a
--foldLeftM f a (x:xs)
--    = (f a x) >>= \b -> do bs <- foldLeftM f a xs
--                           return (b : bs)


--
-- Excercise 10
--
-- foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
--
-- foldl implementation:
-- foldl f a [] = a
-- foldl f a (x:xs) = foldl f (f a x) xs
-- 
-- let rec fold_right f a lst = match lst with
--   | [] -> a
--   | x :: xs -> f x (fold_right f a xs);;
--
-- let rec fold_left f a lst = match lst with
--   | [] -> a
--   | x :: xs -> fold_left f (f a x) xs
--

foldl' f a [] = a
foldl' f a (x:xs) = foldl f (f a x) xs

foldr' f a [] = a
foldr' f a (x:xs) = f x (foldr' f a xs)

-- Main> foldl' (flip (:)) [] "foo"
-- "oof"
-- Main> foldr' (:) [] "foo"
-- "foo"
 
-- got it correct:
foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
foldRightM f a [] = return a
foldRightM f a (x:xs) = (\z -> f x z) =<< foldRightM f a xs 
    where (=<<) = flip (>>=)

--foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
--foldRightM f a [] = return a
--foldRightM f a (x:xs)
--    = do r <- f x (foldRightM f a xs)
--         return r
--
--foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
--foldRightM f a [] = return a
--foldRightM f a (x:xs)
--    = do bs <- f a xs
--         r <- f x bs
--         return r

--foldLeftM f a [] = return a
--foldLeftM f a (x:xs)
--    = f a x >>= \r -> foldLeftM f r xs


