--
-- Excercise 0
--

-- parse item "hello"
-- -> [('h',"ello")]

--
-- Excercise 1
--

-- return 1 +++ return 2
-- -> Always succeeds


--
-- Excercise 2
--

-- Parsing> parse (return 1) "hello"
-- [(1,"hello")]


--
-- Excercise 3
--

-- Parsing> parse (item +++ return 'a') "hello"
-- [('h',"ello")]


--
-- Excercise 4
--
-- To make the type Parser (week X slide Y) a proper Monad we need to define the
-- (>>=) (bind) operator and provide the required instance declaration for the
-- Monad class such that we can use the do notation that was illustrated in the
-- lectures.
--
--

--newtype Parser a                = P (String -> [(a, String)])

--instance Monad Parser where
--    return v                    = P (\inp -> [(v, inp)])
--    p >>= f                     = P (\inp -> case parse p inp of
--                                        [] -> []
--                                        [(v, out)] -> parse (f v) out)

--
-- Excercise 5
--
-- The parser char 'a' +++ return 'b'
-- -> Always succeeds


--
-- Excercise 6
--
-- Given the following implementation of the parser nat, that parses a sequence
-- of one or more digits: 
--
-- nat :: Parser Int
-- nat = do xs <- many1 digit
--       return (read xs)
--
-- Define a parser int :: Parser Int that parses an integer literal.
-- An integer literal consists of an optional minus sign, followed by a sequence
-- of one or more digits.
--
-- Note: "-007" should parse as a valid integer according to this specification,
-- but the resulting value is -7, just like Hugs does
--


