--
-- Excercise 0
--
xs = [1, 2, 3, 4, 5, 6]

--halve1 xs = (take n xs, drop n xs)
--    where n = length xs / 2

halve2 xs = splitAt (length xs `div` 2) xs

halve3 xs = (take (n `div` 2) xs, drop (n `div` 2) xs)
    where n = length xs

--halve4 xs = splitAt (length xs `div` 2)

--halve5 xs = (take n xs, drop (n + 1) xs)
--    where n = length xs `div` 2

halve6 xs = splitAt (div (length xs) 2) xs

--halve7 xs = splitAt (length xs / 2) xs

halve8 xs = (take n xs, drop n xs)
    where n = length xs `div` 2

--
-- Excercise 1
--

safetail1 xs = if null xs then [] else tail xs

safetail2 [] = []
safetail2 (_ : xs) = xs

--safetail3 (_ : xs)
--    | null xs = []
--    | otherwise = tail xs

safetail4 xs
    | null xs = []
    | otherwise = tail xs

--safetail5 xs = tail xs
--safetail5 [] = []

safetail6 [] = []
safetail6 xs = tail xs

--safetail7 [x] = [x]
--safetail7 (_ : xs) = xs

safetail8
    = \ xs ->
        case xs of
            [] -> []
            (_ : xs) -> xs


--
-- Excercise 2
-- 
 
False `f1` False = False
_ `f1` _ = True

--
False `f2` b = b
True `f2` _ = True

--
b `f3` c
    | b == c = True
    | otherwise = False

--
b `f4` c
    | b == c = b
    | otherwise = True

--
b `f5` False = b
_ `f5` True = True

--
b `f6` c
    | b == c = c
    | otherwise = True

--
b `f7` True = b
_ `f7` True = True

--
False `f8` False = False
False `f8` True = True
True `f8` False = True
True `f8` True = True

{-
[True `f1` True == True, True `f1` False == True, False `f1` False == False, False `f1` True == True]
[True `f2` True == True, True `f2` False == True, False `f2` False == False, False `f2` True == True]
[True `f3` True == True, True `f3` False == True, False `f3` False == False, False `f3` True == True]
[True `f4` True == True, True `f4` False == True, False `f4` False == False, False `f4` True == True]
[True `f5` True == True, True `f5` False == True, False `f5` False == False, False `f5` True == True]
[True `f6` True == True, True `f6` False == True, False `f6` False == False, False `f6` True == True]
[True `f7` True == True, True `f7` False == True, False `f7` False == False, False `f7` True == True]
[True `f8` True == True, True `f8` False == True, False `f8` False == False, False `f8` True == True]
-}


-- Excercise 4

-- ok
True `a1` True = True
_ `a1` _ = False

-- ok
a `a2` b = if a then if b then True else False else False

-- not ok
a `a3` b = if not (a) then not (b) else True

-- not ok
--a `a4` b = if a then b

-- not ok
a `a5` b = if a then if b then False else True else False

-- not ok
a `a6` b = if a then if b then False else True else False

-- ok
a `a7` b = if a then b else False

-- ok
a `a8` b = if b then a else False

{-
[True `a1` True == True, True `a1` False == False, False `a1` False == False, False `a1` True == False]
[True `a2` True == True, True `a2` False == False, False `a2` False == False, False `a2` True == False]
[True `a3` True == True, True `a3` False == False, False `a3` False == False, False `a3` True == False]
[True `a4` True == True, True `a4` False == False, False `a4` False == False, False `a4` True == False]
[True `a5` True == True, True `a5` False == False, False `a5` False == False, False `a5` True == False]
[True `a6` True == True, True `a6` False == False, False `a6` False == False, False `a6` True == False]
[True `a7` True == True, True `a7` False == False, False `a7` False == False, False `a7` True == False]
[True `a8` True == True, True `a8` False == False, False `a8` False == False, False `a8` True == False]
-}


-- Excercise 4
-- 
-- Show how the curried function definition mult x y z = x * y * z can be
-- understood in terms of lambda expressions.
--

-- not ok
mult1 x y z = \x -> (\y -> (\z -> x * y * z))

-- not ok
--mult2 = \x -> (x * \y -> (y * \z -> z))

-- ok
mult3 = \x -> (\y -> (\z -> x * y * z))

-- not ok
--mult4 = ((((\x -> \y) -> \z) -> x *) * z)


-- Excercise 7
-- 
-- Choose the correct implementation for the function remove :: Int -> [a] ->
-- [a] which takes a number n and a list and removes the element at position n
-- from the list. 
--

-- not ok
remove1 n xs = take n xs ++ drop n xs

-- not ok
remove2 n xs = drop n xs ++ take n xs

-- not ok
remove3 n xs = take (n + 1) xs ++ drop n xs

-- ok
remove4 n xs = take n xs ++ drop (n + 1) xs

-- Excercise 8
--
-- What is the output of the function call funct 3 [1, 2, 3, 4, 5, 6, 7]? 
-- The function funct is defined as:
funct :: Int -> [a] -> [a]
funct x xs = take (x + 1) xs ++ drop x xs
