-- Excercise 1
n = a `div` length xs
    where a = 10
          xs = [1, 2, 3, 4, 5]

-- Excercise 2
xs = [1, 2, 3, 4, 5]
last xs = head (drop (length xs - 1) xs)
last xs = xs !! (length xs - 1)
last xs = head (reverse xs)

-- Excercise 3
init xs = reverse (tail (reverse xs))

-- Excercise 6
myproduct [] = 1
myproduct (x : xs) = x * myproduct xs

-- Excercise 7
-- How should the definition of the function qsort be modified so that it
-- produces a reverse sorted version of a list? 
sortxs = [5, 1, 2, 2, 12, 3, 4, 4] 
qsort1 [] = []
qsort1 (x : xs) = qsort1 larger ++ [x] ++ qsort1 smaller
    where smaller = [a | a <- xs, a <= x]
          larger = [b | b <- xs, b > x]

qsort2 [] = []
qsort2 (x : xs) = reverse (qsort2 smaller ++ [x] ++ qsort2 larger)
    where smaller = [a | a <- xs, a <= x]
          larger = [b | b <- xs, b > x]

qsort3 [] = []
qsort3 xs = qsort3 larger ++ qsort3 smaller ++ [x]
    where x = minimum xs
          smaller = [a | a <- xs, a <= x]
          larger = [b | b <- xs, b > x]

qsort4 [] = []
qsort4 (x : xs) = reverse (qsort4 smaller) ++ [x] ++ reverse (qsort4 larger)
    where smaller = [a | a <- xs, a <= x]
          larger = [b | b <- xs, b > x]

qsort5 [] = []
qsort5 (x : xs) = qsort5 larger ++ [x] ++ qsort5 smaller
    where smaller = [a | a <- xs, a > x || a == x]
          larger = [b | b <- xs, b < x]

qsort6 [] = []
qsort6 (x : xs) = qsort6 larger ++ [x] ++ qsort6 smaller
    where smaller = [a | a <- xs, a < x]
          larger = [b | b <- xs, b > x]

qsort7 [] = []
qsort7 (x : xs) = reverse (reverse (qsort7 smaller) ++ [x] ++ reverse (qsort7 smaller))
    where smaller = [a | a <- xs, a <= x]
          larger = [b | b <- xs, b > x]


qsort8 [] = []
qsort8 xs = x : qsort8 larger ++ qsort8 smaller
    where x = minimum xs
          smaller = [a | a <- xs, a < x]
          larger = [b | b <- xs, b >= x]

-- code from programming in haskell
pairs :: [a] -> [(a, a)]
pairs xs = zip xs (tail xs)

sorted :: Ord a => [a] -> Bool
sorted xs = and [x < y | (x, y) <- pairs xs]




