--
-- Excercise 0
--
-- Using a list comprehension, define a function that selects all the even numbers from a list
--
-- evens :: [Integer] -> [Integer]
--
-- Example: evens [2, 5, 6, 13, 32] = [2, 6, 32]
--

--evens :: [Integer] -> [Integer]
evens [] = []
evens (x : xs ) = [x | x <- xs, even x]

--evens (x : xs) = even x then x : evens xs else evens xs


--
-- Excercise 3
--
-- Using a list comprehension, define a function squares that takes a non-bottom 
-- Integer n >= 0 as its argument and returns a list of the numbers [1..n] squared. 
--
-- squares 4 = [1*1, 2*2, 3*3, 4*4]
-- squares 0 = []
--

--squares :: Integer -> [Integer]
squares :: (Num a, Ord a, Enum a) => a -> [a]
squares n
    | n < 0 = error "squares: called with negative number"
    | otherwise = [x * x | x <- [1..n]]

--
-- Excercise 4
--
-- Using the squares function that you have implemented, we can define a function 
-- sumSquares :: Integer -> Integer as follows:
--

sumSquares n = sum (squares n)
-- sumSquares 50 = 42925


--
-- Excercise 5
--
-- Modify the previous definition of squares such that it now takes two non-bottom 
-- Integer arguments, m >= 0 and n >= 0 and returns a list of the m square numbers 
-- that come after the first n square numbers. 
--
--

--squares' :: Integer -> Integer -> [Integer]
squares' :: Int -> Int -> [Int]
squares' m n
    | n < 0 || m < 0 = error "squares: called with negative number"
    | otherwise = drop n (squares (n+m))

-- We can define a new sumSquares' function as follows: ???
sumSquares' x = sum . uncurry squares' $ (x, x)
--sumSquares' 50 = 295425 


--
-- Excercise 8
--
-- Using a list comprehension, define a function coords :: Integer -> Integer -> [(Integer, Integer)] 
-- that returns a list of all coordinate pairs on an [0..m] � [0..n] rectangular grid, where m and n 
-- are non-bottom Integers >= 0
--
-- coords 1 1 = [(0,0), (0,1), (1,0), (1,1)]
-- coords 1 2 = [(0,0), (0,1), (0,2), (1,0), (1, 1), (1, 2)]
--

coords :: Integer -> Integer -> [(Integer, Integer)]
coords m n 
    | n < 0 || m < 0 = error "coords: called with negative number"
    | otherwise = [ (x, y) | x <- [0..m], y <- [0..n] ]


