--
-- Excercise 1
--
-- all :: (a -> Bool) -> [a] -> Bool
--

-- ok
all1 p xs = and (map p xs)

-- not ok
--all2 p xs = map p (and xs)

-- ok
all3 p = and . map p

-- ok
all4 p = not . any (not . p)

-- not ok
--all5 p = map p . and

-- ok
all6 p xs = foldl (&&) True (map p xs)

-- not ok
all7 p xs = foldr (&&) False (map p xs)

--
all8 p = foldr (&&) True . map p


--
-- Excercise 2
--
-- any :: (a -> Bool) -> [a] -> Bool
--

-- not ok
--any1 p = map p . or

-- ok
any2 p = or . map p

-- ok
any3 p xs = length (filter p xs) > 0

-- ok 
any4 p = not . null . dropWhile (not . p)

-- not ok
any5 p = null . filter p

-- ok
any6 p xs = not (all (\x -> not (p x)) xs)

-- ok
any7 p xs = foldr (\x acc -> (p x) || acc) False xs

-- not ok
any8 p xs = foldr (||) True (map p xs)


--
-- Excercise 3
--
-- takeWhile :: (a -> Bool) -> [a] -> [a]
--

-- ok
take2 _ [] = []
take2 p (x : xs)
    | p x = x : take2 p xs
    | otherwise = []

-- not ok
take3 _ [] = []
take3 p (x : xs) 
    | p x = take3 p xs 
    | otherwise = [] 

-- not ok
-- XXX: how to do that with foldr?
take4 p = foldl (\acc x -> if p x then x : acc else acc) []


--
-- Excercise 4
--
-- dropWhile :: (a -> Bool) -> [a] -> [a] 
--

-- ok
drop1 _ [] = []
drop1 p (x : xs)
    | p x = drop1 p xs
    | otherwise = x : xs

-- not ok
drop2 _ [] = []
drop2 p (x : xs)
    | p x = drop2 p xs
    | otherwise = xs

-- not ok
drop3 p = foldr (\x acc -> if p x then acc else x : acc) []

-- not ok
drop4 p = foldl add []
    where add [] x = if p x then [] else [x]
          add acc x = x : acc

--
-- Excercise 5
--
-- map :: (a -> b) -> [a] -> [b]
--

-- not ok
map1 f = foldr (\x xs -> xs ++ [f x]) []

-- not ok
map2 f = foldr (\x xs -> f x ++ xs) []

-- not ok
map3 f = foldl (\xs x -> f x : xs) []

-- ok
map4 f = foldl (\xs x -> xs ++ [f x]) []


--
-- Excercise 6
--
-- filter :: (a -> Bool) -> [a] -> [a]
--

-- not ok
filter1 p = foldl (\xs x -> if p x then x : xs else xs) []

-- ok
filter2 p = foldr (\x xs -> if p x then x : xs else xs) []

-- not ok
filter3 p = foldr (\x xs -> if p x then xs ++ [x] else xs) []

-- not ok
--filter4 p = foldl (\x xs -> if p x then xs ++ [x] else xs) []


--
-- Excercise 7
--
-- dec2int :: [Integer] -> Integer
--

-- ok
dec2int = foldl (\x y -> 10 * x + y) 0


--
-- Excercise 8
--
-- Choose an explanation for why the following definition of sumsqreven is
-- invalid: 
--

compose :: [a -> a] -> (a -> a)
compose = foldr (.) id

-- does not type check:
--sumsqreven = compose [sum, map (^2), filter even]

--
-- Excercise 9
--
-- curry :: ((a, b) -> c) -> a -> b -> c
--

-- not ok
curry1 f = \x y -> f x y

-- not ok
curry2 f = \x y -> f

-- ok
-- return a lambda that takes x and y and
-- calls f with a pair made of x and y
curry3 f = \x y -> f (x, y)

-- not ok
curry4 f = \(x, y) -> f x y


--
-- Excercise 10
--
-- uncurry :: (a -> b -> c) -> (a, b) -> c
--

uncurry1 f = \(x, y) -> f x y


--
-- Excercise 11
--
-- Consider the following higher-order function 
-- unfold :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> [a] 
-- that encapsulates a simple pattern of recursion for producing a list.
--

--
-- The function unfold p h t x produces the empty list if the predicate 
-- p x is True. Otherwise it produces a non-empty list by applying the 
-- function h x to give the head of the generated list, and the function 
-- t x to generate another seed that is recursively processed by unfold 
-- to produce the tail of the generated list.
--
unfold :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> [a]
unfold p h t x
    | p x = []
    | otherwise = h x : unfold p h t (t x)

--
-- For example, the function int2bin, that converts a non-negative integer 
-- into a binary number, with the least significant bit first, can be defined as:
--

type Bit = Int

int2bin1 :: Int -> [Bit]
int2bin1 0 = []
int2bin1 n = n `mod` 2 : int2bin1 (n `div` 2)

-- This function can be rewritten more compactly using unfold as follows:

int2bin :: Int -> [Bit]
int2bin = unfold (== 0) (`mod` 2) (`div` 2)

-- Next consider the 
-- function chop8 :: [Bit] -> [[Bit]] 
-- that takes a list of bits and chops it into lists of at most eight bits 
-- (assuming the list is finite, non-partial, and does not contain bottom):

chop81 :: [Bit] -> [[Bit]]
chop81 [] = []
chop81 bits = take 8 bits : chop81 (drop 8 bits)

--
-- chop8 using unfold
--
chop8 :: [Bit] -> [[Bit]]
chop8 = unfold null (take 8) (drop 8) 


--
-- Excercise 12
--
-- map :: (a -> b) -> [a] -> [b]
--

unfoldMap :: (a -> b) -> [a] -> [b]
unfoldMap f = unfold null (f . head) tail 


--
-- Excercise 13
--
-- iterate :: (a -> a) -> a -> [a] 
--

unfoldIterate :: (a -> a) -> a -> [a]
unfoldIterate f = unfold (\x -> False) id f


